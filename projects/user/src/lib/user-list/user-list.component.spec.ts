import { TestBed, async } from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {UserListComponent} from './user-list.component';
import {UserModule} from '../user.module';
import {HttpClientModule} from '@angular/common/http';

describe('UserListComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ UserModule, HttpClientModule ]
    }).compileComponents();
  }));

  it('should create the user list', () => {
    const fixture = TestBed.createComponent(UserListComponent);
    const userList = fixture.debugElement.componentInstance;
    expect(userList).toBeTruthy();
  });

});
